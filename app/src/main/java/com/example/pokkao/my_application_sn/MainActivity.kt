package com.example.pokkao.my_application_sn

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseReference



class MainActivity : AppCompatActivity() {
    var fragment1 = mainFragment()
    var fragment2 = secondFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//      Log.i("Activity state","onCreate")
//        setFragment()

        val mRootRef = FirebaseDatabase.getInstance().reference

        val mUserRef : DatabaseReference = mRootRef.child("users")
        val mMessageRef:DatabaseReference = mRootRef.child("messages")
        
        mUserRef.child("id-12346").setValue("Kai")
//        button_1.setOnClickListener {
//            supportFragmentManager.beginTransaction()
//                    .replace(R.id.main_layout, fragment1)
//                    .addToBackStack(null)
//                    .commit()
//        }
//
//        button_2.setOnClickListener {
//            supportFragmentManager.beginTransaction()
//                    .replace(R.id.main_layout, fragment2)
//                    .addToBackStack(null)
//                    .commit()
//        }
    }

//    fun setFragment(){
//        supportFragmentManager.beginTransaction()
//                .add(R.id.main_layout, fragment1)
////                .addToBackStack("null")
//                .commit()
//    }

    override fun onStart() {
        super.onStart()
        Log.i("Activity state","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("Activity state","onResume")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i("Activity state","onRestart")
    }
    override fun onPause() {
        super.onPause()
        Log.i("Activity state","onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("Activity state","onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("Activity state","onDestroy")
    }


}
